angular.module('app').controller('mvMainCtrl', function($scope) {
    //$scope.myVar = "Hello Angular";
    $scope.courses = [
        {name: 'C# for Sociopaths', featured: true, published: new Date('1/1/2009')},
        {name: 'C# for Main-stream folks', featured: true, published: new Date('1/1/2009')},
        {name: 'Super Duper Expert C#', featured: false, published: new Date('8/31/2011')},
        {name: 'Pedantic C#', featured: false, published: new Date('10/31/2013')}
    ]
});